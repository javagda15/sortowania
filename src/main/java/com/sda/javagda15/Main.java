package com.sda.javagda15;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
//        int rozmiar = 1000000;
//        int[] tablica = new int[rozmiar];
//        for (int i = 0; i < rozmiar; i++) {
//            tablica[i] = new Random().nextInt(10000);
//        }
//        int[] tablica = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] tablica = new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
//        int[] tablica = new int[]{1, 3, 5, 7, 9, 10, 8, 6, 4, 2};
//        int[] tablica = new int[]{2, 4, 6, 8, 10, 9, 7, 5, 3, 1}; //n^2

        System.out.println(Arrays.toString(tablica));

        // sortowanie tablicy
//        BubbleSort.sort(tablica);  // n^2
//        InsertionSort.sort(tablica); // n -> n^2 (if sorted reversely)
//        CountingSort.sort(tablica);
//        MergeSort.sort(tablica);
        QuickSort.sort(tablica);

        // wypisanie tablicy
        System.out.println(Arrays.toString(tablica));
    }
}

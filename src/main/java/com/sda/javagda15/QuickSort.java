package com.sda.javagda15;

public class QuickSort {
    private static int licznikInstrukcji;

    public static void sort(int[] tab) {
        licznikInstrukcji = 0;

        quick(tab, 0, tab.length - 1);

        System.out.println("Licznik instrukcji: " + licznikInstrukcji);
    }

    private static void quick(int[] tab, int from, int to) {
        int middle = (from + to) / 2;

        int pivot = tab[middle];
        int left = from;
        int right = to;

        do {
            while (tab[left] < pivot) {
                licznikInstrukcji++;
                left++;
            }
            while (tab[right] > pivot) {
                licznikInstrukcji++;
                right--;
            }
            if (left <= right) {
                int tmp = tab[left];
                tab[left] = tab[right];
                tab[right] = tmp;
                left++;
                right--;
            }
            licznikInstrukcji++;
        } while (left <= right);

        if (from < right) {
            quick(tab, from, right);
        }
        if (left > to) {
            quick(tab, left, to);
        }
    }
}

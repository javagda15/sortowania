package com.sda.javagda15;

public class InsertionSort {
    public static void sort(int[] tab) {
        int licznikInstrukcji = 0;
        // sortujemy

        int n = tab.length;

        for (int i = 1; i < n; i++) {

            int klucz = tab[i];

            int j = i - 1;

            while (j >= 0) {
                licznikInstrukcji++;
                if (tab[j] > klucz) {
                    tab[j + 1] = tab[j];
                    j--;
                }else{
                    break;
                }
            }
            tab[j + 1] = klucz;
        }

        System.out.println(licznikInstrukcji);
    }
}

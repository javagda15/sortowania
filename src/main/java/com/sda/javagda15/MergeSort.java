package com.sda.javagda15;

import java.util.Arrays;

public class MergeSort {
    private static int licznikInstrukcji;

    public static void sort(int[] tab) {
        licznikInstrukcji = 0;

        mergeSort(tab, 0, tab.length - 1);

        System.out.println("Licznik instrukcji: " + licznikInstrukcji);
    }

    private static void mergeSort(int[] tab, int from, int to) {
        if (from == to) {
            return;
        }

        int middle = (from + to) / 2;

        // przykład: 0-9
        // 0-4 / 5-9
        mergeSort(tab, from, middle);
        mergeSort(tab, middle + 1, to);

        merge(tab, from, middle, to);
    }

    private static void merge(int[] tab, int from, int middle, int to) {

        int[] kopia = Arrays.copyOf(tab, tab.length);

        int left = from;
        int right = middle + 1;

        int index = from;

        while (left <= middle && right <= to) {
            licznikInstrukcji++;
            if (kopia[left] < kopia[right]) {
                tab[index] = kopia[left++];
            } else /*if (kopia[left] >= kopia[right])*/ {
                tab[index] = kopia[right++];
            }
            index++;
        }

        while (left <= middle) {
            licznikInstrukcji++;
            tab[index++] = kopia[left++];
        }

        while (right <= to) {
            licznikInstrukcji++;
            tab[index++] = kopia[right++];
        }
    }
}

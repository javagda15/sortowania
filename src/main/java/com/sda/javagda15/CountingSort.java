package com.sda.javagda15;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CountingSort {
    public static void sort(int[] el) {
        OptionalInt max = Arrays.stream(el)
                .boxed()            // Stream
                .mapToInt(p -> p)   // IntStream
                .max();             // max

        int[] tab = new int[max.getAsInt() + 1];

        for (int i = 0; i < el.length; i++) {
            // el - tablica elementów
            // el[i] - wartość elementu na i'tej pozycji
            // tab - tablica zliczeń
            // tab[i] - ilość zliczeń wartości 'i'
            tab[el[i]]++;
        }

        int licz = 0;

        for (int i = 0; i < tab.length; i++) {
            while (tab[i] > 0) {
                el[licz++] = i;
                tab[i]--;
            }
        }
    }
/*
el -> 1 2 0 0 3 0 2 1 1 0 0 1 1 2 3 3 3 3 0 1
i  -> 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0

tab[el[i]]++;

tab
X| 0 1 2 3
0| 0 0 0 0
1| 0 1 0 0
2| 0 1 1 0
3| 1 1 1 0
4| 2 1 1 0
5| 2 1 1 1
6| 3 1 1 1
7| 3 1 2 1
8| 3 2 2 1
9| 3 3 2 1
0| 4 3 2 1
1| 5 3 2 1
2| 5 4 2 1
3| 5 5 2 1
4| 5 5 3 1
5| 5 5 3 2
6| 5 5 3 3
7| 5 5 3 4
8| 5 5 3 5
9| 6 5 3 5
0| 6 6 3 5


0 0 0 0 0 0 1 1 1 1 1 1 2 2 2 | 3 3 3 3 3
0 0 0 0
*/
}

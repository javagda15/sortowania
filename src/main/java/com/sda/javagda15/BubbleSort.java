package com.sda.javagda15;

public class BubbleSort {
    public static void sort(int[] tab) {
        int ilośćInstrukcji = 0;

        int n = tab.length;

        do {
            for (int i = 0; i < n - 1; i++) {
                ilośćInstrukcji++;
                if (tab[i] > tab[i + 1]) {
                    // swap
                    int tmp = tab[i];
                    tab[i] = tab[i + 1];
                    tab[i + 1] = tmp;
                }
            }
            n = n - 1;
        } while (n > 1);

        System.out.println("Instrukcji: " + ilośćInstrukcji);
    }
}
